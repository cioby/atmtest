<?php

namespace App\Util;

use Symfony\Component\Config\Definition\Exception\Exception;

class ATM
{
    private array $bankNotes = [];
    private int $amount = 0;

    private array $result = [];

    /**
     * @return array
     */
    final public function getBankNotes(): array
    {
        return $this->bankNotes;
    }

    /**
     * @param array $bankNotes
     */
    final public function setBankNotes(array $bankNotes): ATM
    {
        rsort($bankNotes);
        $this->bankNotes = $bankNotes;
        return $this;
    }

    /**
     * @param int $amount
     * @return ATM
     */
    final public function setAmount(int $amount): ATM
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     * @return int
     */
    final public function getAmount(): int
    {
        return $this->amount;
    }

    final public function process() :void
    {
        $amount = $this->amount;
        foreach ($this->bankNotes as $bankNoteValue) {
            $bankNoteCount = intdiv($amount, $bankNoteValue);
            $this->result[$bankNoteValue] = $bankNoteCount;
            $amount -= ($bankNoteCount * $bankNoteValue);
        }
        if($amount > 0){
            throw new \RuntimeException('left over amount over zero');
        }
    }

    /**
     * @return array
     */
    final public function getResult(): array
    {
        return $this->result;
    }

}

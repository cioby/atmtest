<?php

namespace App\Controller;

use ApiPlatform\Metadata\ApiResource;
use App\Entity\Project;
use App\Entity\Withdrawal;
use App\Util\ATM;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use function Symfony\Component\DependencyInjection\Loader\Configurator\env;

/**
 * @Route("/api", name="api_")
 */
class ATMController extends AbstractController
{

    /**
     * @Route("/atm/withdraw", name="project_create", methods={"POST","GET"})
     * @param Request $request
     * @param ManagerRegistry $doctrine
     * @return JsonResponse
     */
    final public function create(Request $request,ManagerRegistry $doctrine): JsonResponse
    {
        $amount = (int)$request->get('amount');
        $bankNotes = [50,20,10,5] ;
        if( $amount % 10){
            return $this->json(['the amount needs to be a multiple of 10']);
        }
        $atm = new ATM();
        $atm->setAmount($amount);
        $atm->setBankNotes($bankNotes);
        $atm->process();
        $result = [
            'amount'=>$amount,
            'notes'=>$atm->getResult()
        ];
        $entityManager = $doctrine->getManager();
        $record= new Withdrawal();
        $record->setAmount($amount);
        $record->setBankNotes($bankNotes);
        $record->setResult($atm->getResult());
        $entityManager->persist($record);
        $entityManager->flush();
        return $this->json($result);
    }
}
